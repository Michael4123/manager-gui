package conventions;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

public class StringPacker implements ServerData {
	
	String s;
	
	public StringPacker(String s)	{
		this.s = s;
	}
	/**
	 * 
	 * @param sarr array of strings
	 * @return packed string of strings
	 * @throws IOException 
	 */
	public void send(DataOutputStream out) throws IOException
	{
		byte[] bytes = ByteBuffer.allocate(4).putInt(s.length()).array();
		for (int i = bytes.length - 1; i >= 0; --i)	{
			out.writeByte(bytes[i]);
		}
		out.writeBytes(s);
	}
	
	public static void send_int(int num, DataOutputStream out) throws IOException	{
		byte[] bytes = ByteBuffer.allocate(4).putInt(num).array();
		for (int i = bytes.length - 1; i >= 0; --i)	{
			out.writeByte(bytes[i]);
		}
	}
	@Override
	public Object getVal() {
		return s;
	}
	
	public static String getVal(InputStream in) throws Exception {
		int len = IntegerPacker.getVal(in);
		if (len > DIRECTORY_DATA.MAX_LEN || len < 0)	{
			throw new Exception();
		}
		byte[] data = new byte[len];
		in.read(data, 0, len);
		String s = new String(data, "UTF-8");
		return s;
	}
	@Override
	public int get_length() {
		return s.length() + 4;
	}
}
