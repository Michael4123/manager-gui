/**
 * 
 */
package conventions;

import java.io.DataOutputStream;
import java.io.IOException;

/**
 * @author michael isjaki
 *
 */
public interface ServerData {
	void send(DataOutputStream out) throws IOException;
	Object getVal();
	int get_length();
}
