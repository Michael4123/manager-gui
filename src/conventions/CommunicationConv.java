package conventions;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.nio.ByteBuffer;

public class CommunicationConv	
{
	static final int LOGIN_OK = 0;
	static final int WRNG_PAS = 1;
	static final int USER_NEX = 2;
	
	static public void send(ServerData[] s, boolean get_sock, Object[] out) throws IOException
	{
		Socket clientSocket = new Socket(DIRECTORY_DATA.IP, DIRECTORY_DATA.PORT);
		DataOutputStream outToServer = 
				new DataOutputStream(clientSocket.getOutputStream());
		
		/*
		 * ssl wrap
		 */
		int len = 0;
		for (ServerData var : s)	{
			len += var.get_length();
		}
		ShortPacker ip = new ShortPacker(len);
		ip.send(outToServer);
		
		
		for (ServerData var : s)	{
			var.send(outToServer);
		}
		
		if (get_sock)	{
			out[0] = clientSocket;
		}	else	{
			int s1 = recv(clientSocket.getInputStream());
			clientSocket.close();
			out[0] = s1;	
		}
	}

	static public int send(ServerData[] s) throws Exception	{
		Integer[] out = new Integer[1];
		send(s, false, out);
		System.out.println(out);
		return out[0];
	}
	
	// This function is a bit redundant, but receiving is a whole
	// new operation, so I decided to dedicate an entire function
	// to this purpose
	/**
	 * 
	 * @param sock input stream
	 * @return the byte indicating exit_status
	 * @throws IOException in case of problems with read
	 */
	static public int recv(InputStream sock) throws IOException	{
		byte b[] = new byte[2];
		b[1] = (byte) sock.read();
		b[0] = (byte) sock.read();
		
		return b[0]*256 + b[1];
	}
	
	static public String ip_conv(int ip)	{
		byte[] ins = new byte[4];
		for (int i = 0; i < 4; ++i)	{
			ins[i] = (byte) (ip % 256);
			ip /= 256;
		}
		
		return ip_conv(ins);
	}
	
	static public String ip_conv(byte[] ins)
	{
		StringBuilder ip_s = new StringBuilder();
		for (int i = 0; i < 4; ++i)	{
			ip_s.append(Integer.toString((ins[i] + 256) % 256));
			ip_s.append(".");
		}
		ip_s.deleteCharAt(ip_s.length() - 1);
		return ip_s.toString();
	}
	
	static public int ip_conv(String ip)	{
		byte[] ins = new byte[4];
		int i = 3;
		for (String s : ip.split("\\."))	{
			ins[i--] = (byte) Integer.parseInt(s);
		}
		return ByteBuffer.wrap(ins).getInt();
	}
}