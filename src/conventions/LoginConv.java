package conventions;

public class LoginConv {
	public static int tryLogin(String name, String pass) throws Exception
	{
		ServerData[] data = {new ShortPacker(ACTION_ID.LOGIN_ID.getValue()),
				new StringPacker(name),
				new StringPacker(pass)};
		
		return CommunicationConv.send(data);
	}
}
