package conventions;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/**
 * @author michael isjaki
 *
 */
public class IntegerPacker implements ServerData{

	Integer val;
	public IntegerPacker(int val)	{
		this.val = val;
	}
	public IntegerPacker(Integer val)	{
		this.val = val;
	}
	@Override
	public void send(DataOutputStream out) throws IOException {
		byte[] bytes = ByteBuffer.allocate(4).putInt(val).array();
		for (int i = bytes.length - 1; i >= 0; --i)	{
			out.writeByte(bytes[i]);
		}
	}
	
	@Override
	public Object getVal() {
		return val;
	}
	
	public static int getVal(InputStream in) throws IOException
	{
		byte[] b = new byte[4];
		for (int i = 3; i >= 0; --i)	{
			b[i] = (byte) in.read();
		}
		
		return ByteBuffer.wrap(b).getInt();
	}
	@Override
	public int get_length() {
		return 4;
	}

}
