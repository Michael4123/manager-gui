package conventions;

public class RemRouterConv {
	public static int tryRemRouter(String name, String pass, String ip, int port) throws Exception
	{
		int ipv4 = CommunicationConv.ip_conv(ip);
		ServerData[] data = {new ShortPacker(ACTION_ID.REM_ROUTER_ID.getValue()),
				new StringPacker(name),
				new StringPacker(pass),
				new IntegerPacker(ipv4),
				new IntegerPacker(port)};
		
		return CommunicationConv.send(data);
	}
}
