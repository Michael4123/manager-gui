package conventions;

public class RouterBean {
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getDate_val() {
		return date_val;
	}
	public void setDate_val(String date_val) {
		this.date_val = date_val;
	}
	public String getDate_add() {
		return date_add;
	}
	public void setDate_add(String date_add) {
		this.date_add = date_add;
	}
	public String getAdder() {
		return adder;
	}
	public void setAdder(String adder) {
		this.adder = adder;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	String ip;
	String date_val;
	String date_add;
	String adder;
	int port;
	
	@Override
	public String toString()
	{
		StringBuilder s = new StringBuilder();
		s.append(ip + ",");
		s.append(Integer.toString(port) + ",");
		s.append(adder + ",");
		s.append(date_add + ",");
		s.append(date_val);
		
		return s.toString();
	}
	
}
