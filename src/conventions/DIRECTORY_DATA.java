package conventions;

public class DIRECTORY_DATA {
	public static final int MAX_LEN = 256;
	
	public static final String IP = "127.0.0.1";
	public static final int PORT = 40440;
	
	public static final int EXIT_OK = 1;
	public static final int WRONG_PASS = 2;
	public static final int UNKNOWN_USER = 3;
}
