package conventions;

public enum ACTION_ID {
	LOGIN_ID (1),
	ADD_ROUTER_ID (2),
	ADD_ADMIN_ID(3),
	GET_ROUTER_ID (4),
	REM_ROUTER_ID (5);
	
	private final int id;
	ACTION_ID(int id)	{this.id = id;	}
	public int getValue(){return id;}
}
