package conventions;

import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

public class ShortPacker implements ServerData {

	Integer val;
	public ShortPacker(int val)	{
		this.val = val % 65536;
	}
	
	public ShortPacker(Integer val)	{
		this.val = val % 65536;
	}
	
	@Override
	public void send(DataOutputStream out) throws IOException {
		byte[] bytes = ByteBuffer.allocate(4).putInt(val).array();
		out.writeByte(bytes[3]);
		out.writeByte(bytes[2]);
	}
	
	@Override
	public Object getVal() {
		return val;
	}

	@Override
	public int get_length() {
		return 2;
	}

}
