package conventions;

import java.io.InputStream;
import java.net.Socket;
import java.util.ArrayList;

public class GetRoutersConv {
	public static RouterBean[] get_routers_admin(String nam, String pas) throws Exception	{	
		ServerData s_arr[] = {new ShortPacker(ACTION_ID.GET_ROUTER_ID.getValue()),
				new StringPacker(nam),
				new StringPacker(pas)
		};
		
		Socket[] in = new Socket[1];
		try{
		CommunicationConv.send(s_arr, true, in);
		}catch(Exception e)	{
			e.printStackTrace();
			throw e;
		}

	
		
		RouterBean[] routers = null;
		try	{
		routers = recv(in[0].getInputStream());
		}	catch(Exception e)	{
			e.printStackTrace();
			throw e;
		}

		in[0].close();
		return routers;
	}
	
	static public RouterBean[] recv(InputStream in) throws Exception	
	{
		ArrayList<RouterBean> lst = new ArrayList<>();
		int code = IntegerPacker.getVal(in);
		System.out.println(code);
		while (code != 0)	{
			RouterBean rb = new RouterBean();
			// This is a waste of time, but more elegant
			rb.setIp(CommunicationConv.ip_conv(code));
			rb.setPort(IntegerPacker.getVal(in));
			rb.setAdder(StringPacker.getVal(in));
			
			rb.setDate_add(StringPacker.getVal(in));
			System.out.print(1);
			rb.setDate_val(StringPacker.getVal(in));
			
			System.out.println(rb.toString());
			lst.add(rb);
			
			code = IntegerPacker.getVal(in);
		}
		RouterBean[] routers_final = lst.toArray(new RouterBean[lst.size()]);
		return routers_final;
	}
	
	
	
}
