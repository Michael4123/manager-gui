package conventions;


public class AddRoutConv {
	public static int tryAddRouter(String name, String pass, String ip, 
			int port, String pubk) throws Exception
	{
		int ipv4 = CommunicationConv.ip_conv(ip);
		ServerData[] data = {new ShortPacker(ACTION_ID.ADD_ROUTER_ID.getValue()),
				new StringPacker(name),
				new StringPacker(pass),
				new IntegerPacker(ipv4),
				new IntegerPacker(port),
				new StringPacker(pubk)};
		
		return CommunicationConv.send(data);
	}
}
