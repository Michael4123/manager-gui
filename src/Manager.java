

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import conventions.DIRECTORY_DATA;
import conventions.LoginConv;

/**
 * Servlet implementation class Manager
 */
@WebServlet("/Manager")
public class Manager extends HttpServlet {
	private static final long serialVersionUID = 1L;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Manager() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException	{
		System.out.println("get" + resp.getHeader("arc"));
		System.out.println("get" + req.getHeader("arc"));
		HttpSession session = req.getSession(false);
		if (session != null && session.getAttribute("password") != null)	{
			if (req.getParameter("stype") != null && req.getParameter("stype").equals("lo"))	{
				session.invalidate();
			}	else	{
				RequestDispatcher rd = req.getRequestDispatcher("manager.jsp");
				rd.forward(req, resp);
				return;
			}
		}	
		
		// Default behavior
		RequestDispatcher rd = req.getRequestDispatcher("login.jsp");
		rd.forward(req, resp);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println(response.getHeader("arc"));
		System.out.println(request.getHeader("arc"));
		
		String username = request.getParameter("username");
		if (username != null)	{
			username = username.toLowerCase();
		}
		String password = request.getParameter("password");
		
		int exit_code;
		HttpSession session = request.getSession(false);
		if (session != null && session.getAttribute("password") != null)	{
			RequestDispatcher rd = request.getRequestDispatcher("manager.jsp");
			rd.forward(request, response);
			return;
		}
		try {
			exit_code = LoginConv.tryLogin(username, password);
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(503);
			return;
		}
		if (exit_code == DIRECTORY_DATA.EXIT_OK)	{
			session = request.getSession();
			session.setAttribute("password", password);
			session.setAttribute("username", username);
			
			RequestDispatcher rd = request.getRequestDispatcher("manager.jsp");
			rd.forward(request, response);
			
			return;
		}	else if (exit_code == DIRECTORY_DATA.WRONG_PASS)	{
			request.setAttribute("username", username);
			request.setAttribute("code", "wp");
		}	else if (exit_code == DIRECTORY_DATA.UNKNOWN_USER){
			request.setAttribute("code", "un");
		}
		
		RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
		rd.forward(request, response);
		
	}

}
