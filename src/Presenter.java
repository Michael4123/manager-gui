

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import conventions.AddRoutConv;
import conventions.DIRECTORY_DATA;
import conventions.GetRoutersConv;
import conventions.RemRouterConv;
import conventions.RouterBean;

/**
 * Servlet implementation class Presenter
 */
@WebServlet("/Presenter")
@MultipartConfig
public class Presenter extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String ADD_ROUTER = "add_router";
	private static final String REM_ROUTER = "rem_router";
	private static final String GET_ROUTERS = "get_routers";
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Presenter() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String req = request.getParameter("request");
		if (req == null)	{
			req = getPartString(request, "request");
		}
		System.out.println(req);
		HttpSession session = request.getSession(false);
		// He should not be here with no session
		if (session == null) return;
	    
		switch (req)	{
		case ADD_ROUTER:
		    String s = getPartString(request, "fileToUpload");
		    String ip = getPartString(request, "ip");
		    String port = getPartString(request, "port");
		    
		    String uname = (String) session.getAttribute("username");
			String out = addRouter(uname, (String) session.getAttribute("password"),
				ip, port, s);
			
			if (out.equals("ok"))	{
				DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy");
				LocalDateTime now = LocalDateTime.now();
				String nowS = dtf.format(now);
				response.setHeader("arc", ip + "," + port + "," + uname + "," + nowS + "," + nowS);
			}
			
			response.sendRedirect("Manager");
			break;
		case GET_ROUTERS:
			if (session.getAttribute("has-rout") != null)	break;
			String routers = getRoutersAdmin((String) session.getAttribute("username"), 
					(String) session.getAttribute("password"));
			
			Cookie cookie = new Cookie("routers-info", routers);
			response.addCookie(cookie);
			response.getWriter().append(routers);
			session.setAttribute("has-rout", true);
			break;
		case REM_ROUTER:
			remRouter((String) session.getAttribute("username"), (String) session.getAttribute("password"),
					request.getParameter("ip"), request.getParameter("port"), response);
			break;
		default:
			// Do nothing
		}
		
	}
	
	private String addRouter(String name, String pass, String ip, String port,
			String pubk) throws IOException
	{
		int exit_code;
		try {
			exit_code = AddRoutConv.tryAddRouter(name, pass, ip, Integer.parseInt(port), pubk);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			exit_code = -1;
		}
		
		if (exit_code == DIRECTORY_DATA.EXIT_OK)	{
			return "ok";
		}	else	{
			return "nok";
		}
	}
	
	private void remRouter(String name, String pass, String ip, String port,
			HttpServletResponse response) throws IOException
	{
		int exit_code;
		try {
			exit_code = RemRouterConv.tryRemRouter(name, pass, ip, Integer.parseInt(port));
		} catch (Exception e) {
			e.printStackTrace();
			exit_code = -1;
		}
		
		if (exit_code == DIRECTORY_DATA.EXIT_OK)	{
			response.getWriter().write("ok");
		}	else	{
			response.getWriter().write("nop");
		}
	}
	
	/**
	 * 
	 * @param name username for authentication
	 * @param password same for password
	 * @return a string representing the routers, or null on failure
	 * @throws IOException in case of writing error
	 */
	private String getRoutersAdmin(String name, String password) throws IOException
	{
		StringBuilder s = new StringBuilder();
		RouterBean[] rb = null;
		try {
			rb = GetRoutersConv.get_routers_admin(name, password);
		} catch (Exception e) {
			return null;
		}
		for (RouterBean b : rb)	{
			s.append(b.toString() + ":");
		}
		
		return s.toString();
	}
	
	private String getPartString(HttpServletRequest request, String token) throws IllegalStateException, IOException, ServletException
	{
		String req;
		Part t = request.getPart(token);
		if (t == null) return null;
		
		byte[] bs = new byte[t.getInputStream().available()];
		t.getInputStream().read(bs, 0, bs.length);
		req = new String(bs);
		
		return req;
	}
}
