function validateForm()	{
	var un = document.getElementById("username").value;
	var pass = document.getElementById("password").value;
	
	if (un.length > 32 || un.length < 3) return false;
	if (pass.length > 32 || pass.length < 6) return false;
	document.getElementById("signin").disabled = true;
	return true;

		
}

$(document).ready(function() {
	var x = document.getElementById("servletExtra").value;
	if (x === "wp")	{
		document.getElementById("username").value = document.getElementById("usernameExtra").value;
		document.getElementById("passErr").style.visibility = "visible";
	}	else if (x === "un")	{
		document.getElementById("userErr").style.visibility = "visible";
	}	
});

