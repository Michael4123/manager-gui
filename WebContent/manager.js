var addRouter_ajax = new getAjaxPost(addRouter_callback);
var loadRouters_ajax = new getAjaxPost(loadRouters_callback);
var cookies = null;
var action;
$(document).ready(function(){
	cookies = document.cookie;
	$("li" ).last().addClass("last");
	$("#content-main").css("display", "initial");
	$("li").click(function(){
		$("ul li.selected").removeClass("selected");
		$(this).addClass("selected");
	});
	
	$("#tab-main").click(set_content("content-main"));
	$("#tab-managers").click(set_content("content-managers"));
	$("#tab-routers").click(set_content("content-routers"));
	
	$("#add_router").click(function(){
		$("#fileToUpload").css("display", "inline");
		$("#addit_info").css("display", "inline");
		action = "add_router";
	});
	$("#rem_router").click(function(){
		$("#fileToUpload").css("display", "none");
		$("#addit_info").css("display", "inline");
		action = "rem_router";
	});
	
	var temp = cookies.split("; ");
	var res = null;
	for (var i = 0; i < temp.length; ++i)	{
		if (temp[i].startsWith("routers-info"))	{
			var tt = temp[i].split("=");
			res = tt[1].substr(1, tt[1].length - 2);
			break;
		}
	}

	if (res === null)	{
		var params = "request=get_routers";
		loadRouters_ajax.call(params, "Presenter");
	}	else	{
		loadRouters_callback(res);
	}
});

function set_content(name)	{
	return function()
	{
		$(".tabcontent").css("display", "none");
		document.getElementById(name).style.display = "initial";
		
	}
}

function loadRouters_callback(data)	{
	data = data.split(":");
	for (var i = 0; i < data.length; ++i)	{
		if (data[i].length == 0) return;
		add_router_inf(data[i].split(","));
	}
}

function add_router_inf(data)
{
	var newtr = document.createElement("tr");
	for (var i = 0; i < data.length; i++) {
	    var newtd = document.createElement("td");
	    newtd.appendChild(document.createTextNode(data[i]));
	    newtr.appendChild(newtd);
	}

	document.getElementById("routers_table_tbody").appendChild(newtr);
}

function getAjaxPost(callback)	{
	this.xhr = new XMLHttpRequest();
	
	this.xhr.onreadystatechange = function() {
	    if (this.readyState == 4 && this.status == 200) {
	    	var data = this.responseText;
	    	callback(data);
	    }
	}
	
	this.call = function(params, server)	{
		this.xhr.open("POST", server, true);
		
		this.xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
		this.xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	    
		this.xhr.send(params);
	}
}

function addRouter_callback(data)	{
    if (data === "ok"){
    	$("#addit_info").hide();
    	alert(action + ": success");
    }	else	{
    	alert(action + ": failure")
    }
    
    document.getElementById("portf").value = "";
    document.getElementById("ipf").value = "";
}

function validate_ip(ip)	{
	ipa = ip.split(".");
	if (ipa.length != 4)	{
        document.getElementById("ipf").value = "";
		alert("invalid IPv4 address");
		return false;
	}
	for (var i = 0; i < ipa.length; ++i)	{
		if (ipa[i] > 256 || ipa[i] < 0 || ipa[i].length == 0)	{
			alert("invalid IPv4 address");
	        document.getElementById("ipf").value = "";
			return false;
		}
	}
	
	return true;
}

function validate_port(port)
{
	// TODO check if port is an integer!
	if (port < 1 || port >= 65536)	{
		alert("invalid port number");
		document.getElementById("portf").value = "";
		return false;
	}
	return true;
}

function send()	{
	var ip = document.getElementById("ipf").value;
	if (!validate_ip(ip)) return;
	var port = document.getElementById("portf").value;
	if (!validate_port(port)) return;
	
	if (action === "add_router")	{
		if(document.getElementById("fileToUpload").value === "") {
			   alert("You must provide a pk file!");
			   return;
		}	else	{
			$("#rout_form").send();
		}
	}
	var params = "request=" + action;
	params += "&ip=" + ip;
	params += "&port=" + port;

	addRouter_ajax.call(params, "Presenter");
}



