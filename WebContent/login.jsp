<%@ page language="java" contentType="text/html; charset=windows-1255"
    pageEncoding="windows-1255"%>
<%@ page import = "java.io.*,java.util.*, javax.servlet.*" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
	<script src="tester.js"></script>
	<link rel="stylesheet" href="form.css">
	<link rel="icon" href="https://cdn0.iconfinder.com/data/icons/flat-security-icons/512/lock-open-blue.png">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1255">
<title>Tor Login</title>
</head>
<body>
	<div class="outer">
		<div class="middle">
			<div class="inner">
				<form id="form1" action="Manager" method="POST"
				onsubmit="return validateForm()">
					<fieldset>
						<legend>Login information:</legend>
						<div class="inputField">
							<input name="username" class="margin-min" placeholder="Username" type="text" id="username"> 
							<img class="inf" src="https://cdn0.iconfinder.com/data/icons/users-android-l-lollipop-icon-pack/24/user-128.png" >
							<img id="userErr" class="error" src="http://www.freeiconspng.com/uploads/red-error-round-icon-2.png">
						</div>
						<div class="inputField">
							<input name="password" class="margin-min" placeholder="Password" type="password" id="password"> 
							<img class="inf" src="https://cdn4.iconfinder.com/data/icons/standard-free-icons/139/Safety01-128.png" >
							<img id="passErr" class="error" src="http://www.freeiconspng.com/uploads/red-error-round-icon-2.png">
						</div>
						<input class="margin-min" type="submit" value="Login" id="signin">
					</fieldset>
				</form>
				<br>
			</div>
		</div>
	</div>
	<%
	   Date date = new Date();
	   out.print( "<h2 align = \"center\">" +date.toString()+"</h2>");
	   
	%>
	
	<% 
	String s = (String) request.getAttribute("code");
	String us = (String) request.getAttribute("username");
	if (s == null) s = "np";
	if (us == null) us = "";
	out.print("<input type=\"hidden\" id=\"servletExtra\" value=\"" + s + "\">\n");
	out.print("<input type=\"hidden\" id=\"usernameExtra\" value=\"" + us + "\">");
	%>
</body>
</html>