<HTML>
	<HEAD>
		<link rel="stylesheet" href="manager.css">
		<link rel="icon" href="https://lh3.googleusercontent.com/iGAZ0rWvU-gGFiQrI4Vfb9AI3wn2kx0p1n7dtsPX9AJVASx18Bn5m6uGu2Tr8_8n35E=w300-rw">
		<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
		<script type="text/javascript" src="manager.js"></script>
		<TITLE>manager page</TITLE>
	</HEAD>
	<BODY>
		<div class="toolbar">
			<ul id="navbar">
				<li class="selected" id="tab-main">
					<div> main </div>
				</li>
				<li id="tab-routers">
					<div> routers </div>
				</li>
				<li id="tab-managers">
					<div> manager </div>
				</li>
				<li>
					<form method="GET" action="Manager" class="small">
						<input type="hidden" name="stype" value="lo">
						<input type="submit" value="log out" class="no_button">
					</form>
				</li>
			</ul>
		</div>
		
		<div id="content-main" class="tabcontent">
			<h1>Main</h1>
		</div>
		
		<div id="content-managers" class="tabcontent">
			<h1>Managers</h1>
		</div>
		
		<div id="content-routers" class="tabcontent">
			<div class="center">
				<div class="inner">
					<h1>Routers</h1>
					
					<button id="add_router" class="fancy">add router</button>
					<button id="rem_router" class="fancy">remove router</button>
					
					<div id="addit_info">
						<br>
						<form id="rout_form" action="Presenter" method="POST" enctype="multipart/form-data">
							<input type="file" name="fileToUpload" id="fileToUpload"><br>
							<input type="text" name="ip" placeholder="ip" id="ipf">
							<input type="text" name="port" placeholder="port" id="portf">
							<input type="hidden" name="request" value="add_router">
							<button onclick="send()">send</button><br>
						</form>
					</div>
					
					<table id="routers_table">
					  <thead>
					    <tr>
					      <th>IP</th>
					      <th>Port</th>
					      <th>Acknowledged by</th>
					      <th>Acknowledgement date</th>
					      <th>Last Verified</th>
					    </tr>
					  </thead>
					  <tbody id="routers_table_tbody">

					  </tbody>
					</table>
				</div>
			</div>
		</div>
	</BODY>
</HTML>